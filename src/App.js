import React from 'react';
import './App.css';
import Login from './components/Login';
import Dashboard from './components/Dashboard';
import { Router, Route, Switch, Redirect } from 'react-router-dom';
import history from '../src/history';
import CreateTask from './components/CreateTask';
import TaskListDeliverPerson from './components/TaskListDeliverPerson';
import PreviousTasks from './components/PreviousTasks';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.requireAuth = this.requireAuth.bind(this);
  }
  requireAuth() {
    if (localStorage.jwtToken) {
      if(localStorage.is_superuser==='true')
        return <Redirect push to='/dashboard' />;
      else
        return <Redirect push to='/new_tasks_delivery_person' />;

    }
    else {
      return <Redirect push to='/login' />;
    }

  }
  render() {
    return (
      <div className="App">
        <Router history={history}>

          <div>
            <Switch>
              <Route path='/' exact render={this.requireAuth} />
              <Route path="/login" render={() => (
                localStorage.jwtToken ? (<Redirect push to='/' />) : (<Login />)
              )} />
              <Route path="/dashboard" render={() => (
                localStorage.jwtToken && localStorage.is_superuser==='true'? (<Dashboard />) : (<Redirect push to='/' />)
              )} />
              <Route path="/tasks/:id" exact={true} render={() => ( 
                localStorage.jwtToken && localStorage.is_superuser==='true' ? (<CreateTask isEdit={true} />) : (<Redirect push to='/' />)
              )} />
              <Route path="/tasks" render={() => (
                localStorage.jwtToken && localStorage.is_superuser==='true'? (<CreateTask isEdit={false} />) : (<Redirect push to='/' />)
              )} />
              <Route path="/new_tasks_delivery_person" render={() => (
                localStorage.jwtToken && localStorage.is_superuser==='false'? (<TaskListDeliverPerson />) : (<Redirect push to='/' />)
              )} />
              <Route path="/previous_accepted_task" render={() => (
                localStorage.jwtToken && localStorage.is_superuser==='false'? (<PreviousTasks />) : (<Redirect push to='/' />)
              )} />



            </Switch>
          </div>
        </Router>
      </div>
    );
  }

}

export default App;
