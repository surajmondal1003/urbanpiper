import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post('http://surajmondal1003.pythonanywhere.com/login', userData)
    .then(res => {
      // Save to localStorage
      const { token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      localStorage.setItem('is_superuser', res.data.is_superuser);
      // Set token to Auth header
      setAuthToken(token);
    
      dispatch(setCurrentUser(res.data));
    })
    .catch(err =>
      dispatch({
        type: 'GET_ERRORS',
        payload: err.response.data
      })
    );
};

// Set logged in user
export const setCurrentUser = user => {
  return {
    type: 'SET_CURRENT_USER',
    payload: user
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  localStorage.removeItem('is_superuser');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};