import React from 'react';
import { loginUser } from '../actions/auth';
import { connect } from 'react-redux';
import classnames from 'classnames';
import { withRouter } from "react-router";
import isEmpty from '../validation/is-empty';

class Login extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            errors: {}
        };
    }
    onChange = e => {
        this.setState({ [e.target.name]: e.target.value });

    }

    validate() {
        let errors = {};
        let isValid = true;
        if (this.state.username === '') {
            errors.username = 'Username is Required';
            isValid = false;
        }
        if (this.state.password === '') {
            errors.password = 'Password is Required';
            isValid = false;
        }

        if (!isValid) {
            this.setState({ errors: errors });
            return false;
        }
        return true;
    }
    onSubmit = e => {
        e.preventDefault();

        const userData = {
            username: this.state.username,
            password: this.state.password
        };
        if (this.validate()) {
            this.props.loginUser(userData);

        }
    }

    componentDidMount() {

        if (this.props.auth.isAuthenticated && this.props.auth.user.is_superuser === true) {

            this.props.history.push('/dashboard');
        }
        if (this.props.auth.isAuthenticated && this.props.auth.user.is_superuser === false) {
            this.props.history.push('/new_tasks_delivery_person');
        }
    }


    componentWillReceiveProps(nextProps) {

        if (nextProps.auth.isAuthenticated && nextProps.auth.user.is_superuser === true) {

            this.props.history.push('/dashboard');
        }
        if (nextProps.auth.isAuthenticated && nextProps.auth.user.is_superuser === false) {
            this.props.history.push('/new_tasks_delivery_person');
        }
        if (nextProps.errors) {
            this.setState({ errors: nextProps.errors });
        }
    }

    renderError() {
        if (!isEmpty(this.props.errors.errors)) {
            return (
                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong> {this.props.errors.errors.non_field_errors[0]}</strong> 
              </div>
            );
        }
    }
    render() {

        const { errors } = this.state;
        return (
            <div className="login">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">Log In</h1>
                            <p className="lead text-center">
                                Sign in to your UrbanPiper Account
                  </p>
                            <form onSubmit={this.onSubmit.bind(this)}>


                                <div className="form-group">
                                    <input
                                        type="text"
                                        className={classnames('form-control form-control-lg', {
                                            'invalid': errors.username
                                        })}
                                        placeholder="username"
                                        value={this.state.username}
                                        onChange={(e) => this.setState({ username: e.target.value ,errors: {username:''}})} />
                                    <span style={{ color: "red" }}>{errors.username}</span>

                                </div>
                                <div className="form-group">
                                    <input
                                        type="password"
                                        className={classnames('form-control form-control-lg', {
                                            'invalid': errors.password
                                        })}
                                        placeholder="Password"
                                        value={this.state.password}
                                        onChange={(e) => this.setState({ password: e.target.value ,errors: {password:''}})} />
                                    <span style={{ color: "red" }}>{errors.password}</span>
                                </div>

                                <input type="submit" className="btn btn-info btn-block mt-4" />

                                {this.renderError()}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

const mapStateToProps = state => ({

    auth: state.auth,
    errors: state.errors,
});

export default withRouter(connect(mapStateToProps, {
    loginUser
})(Login));