import React from 'react';
import moment from 'moment';
import Navbar from './Navbar';
import setAuthToken from '../utils/setAuthToken';
import axios from 'axios';
import _ from 'lodash';
import isEmpty from '../validation/is-empty';


class PreviousTasks extends React.Component {
    state = { previous_tasks: [], status: '', errors: {}, network_error: {} };

    componentDidMount() {
        setAuthToken(localStorage.getItem('jwtToken'));

        axios
            .get(`/accepted_tasks_list_deliver_person`)
            .then(res => {
                this.setState({ previous_tasks: res.data });

            })
            .catch(err =>
                console.log(err)
            );

    }
    validate(task_id) {
        let errors = {};
        let isValid = true;

        if (this.state.status === '') {
            errors.status = 'Priority is Required';
            errors.id = task_id;
            isValid = false;
        }

        if (!isValid) {
            this.setState({ errors: errors });
            return false;
        }
        return true;
    }
    changeStatus(task_id) {
        if (this.validate(task_id)) {
            let tasks = _.map(this.state.previous_tasks, _.cloneDeep);
            const requiredTaskIndex = _.findIndex(tasks, taskItem => taskItem.id === task_id);
            if (requiredTaskIndex > -1) {
                tasks[requiredTaskIndex] = { ...tasks[requiredTaskIndex], status: this.state.status };
            }

            setAuthToken(localStorage.getItem('jwtToken'));
            axios
                .put(`/tasks_action_deliver_person`, { status: this.state.status, task_id: task_id })
                .then(res => {


                    this.setState({ previous_tasks: tasks }, () => console.log(this.state.tasks));


                })
                .catch(err =>
                    this.setState({ network_error: { message: err.response.data.message } })
                );
        }
    }

    renderActionBtn(task) {
        const { errors } = this.state;
        // if (task.status == 'new' && task.status != 'declined' && task.status != 'cancelled') {
        return (
            <div key={task.id}>
                <select className='form-control form-control-md'

                    onChange={(e) => this.setState({ status: e.target.value, errors: {}, network_error: {} })} >
                    <option value="0">Select</option>
                    <option value="completed">Complete</option>
                    <option value="declined">Decline</option>
                </select>
                {errors.id===task.id?<span style={{ color: "red" }}>{errors.status}</span>:''}
                <br />
                <button type="button" className="btn btn-info  col-md-6" onClick={this.changeStatus.bind(this, task.id)}>Apply</button>

            </div>
        );

        // }
    }
    renderError() {
        if (!isEmpty(this.state.network_error)) {
            return (
                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong> {this.state.network_error.message}</strong>
                </div>


            );
        }
    }
    renderPriority(priority) {
        if (priority === '1')
            return 'LOW';
        else if (priority === '2')
            return 'MEDIUM';
        else if (priority === '3')
            return 'HIGH';
        else
            return 'NO PRIORITY';
    }
    renderList() {

        return this.state.previous_tasks.map(task => {
            return (
                <tr key={task.id}>
                    <td>{task.title}</td>
                    <td>{this.renderPriority(task.priority)}</td>
                    <td>{task.status.toString().toUpperCase()}</td>
                    <td>{moment(task.created_at).format('YYYY-MM-DD')}</td>
                    <td>{this.renderActionBtn(task)}</td>
                </tr>

            );
        });

    }

    render() {
        console.log(this.state);
        return (
            <div>

                <Navbar />
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Task Title</th>
                            <th scope="col">Priority</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Action</th>
                        </tr>

                    </thead>
                    <tbody>{this.renderList()}</tbody>

                </table>
                {this.renderError()}

            </div>
        );
    }

}

export default PreviousTasks;