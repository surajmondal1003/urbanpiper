import React from 'react';
import moment from 'moment';

class TaskStatusList extends React.Component {

    renderList(){
        return this.props.task_history.map((history) => {
            return (
                <tr key={history.id}>
                    <td>{this.props.deliver_person?this.props.deliver_person.username:'Not Yet Accepted'}</td>
                    <td>{history.status.toString().toUpperCase()}</td>
                    <td>{moment(history.created_at).format('YYYY-MM-DD')}</td>
                </tr>
            );
        });
    }


    render() {
        console.log(this.props);
        return (
            <table className="table">
                <thead className="thead-dark">
                    <tr>
                        <th scope="col">Delivery Person</th>
                        <th scope="col">Status</th>
                        <th scope="col">Created At</th>
                    </tr>
                </thead>
                <tbody>
                    {this.renderList()}

                </tbody>
            </table>
        );
    }

}

export default TaskStatusList;