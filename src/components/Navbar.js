import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { logoutUser } from '../actions/auth';
import { withRouter } from "react-router";
class Navbar extends Component {
    onLogoutClick(e) {
        e.preventDefault();

        this.props.logoutUser();
        this.props.history.push('/login');
    }

      
    render() {
    const deliverpersonLinks= (
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
                <div className="container">
                    <Link className="navbar-brand" to="/new_tasks_delivery_person">
                        All Tasks
                    </Link>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#mobile-nav"
                    >
                        <span className="navbar-toggler-icon" />
                    </button>

                    <div className="collapse navbar-collapse" id="mobile-nav">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/new_tasks_delivery_person">
                                    {' '}
                                    See New task
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/previous_accepted_task">
                                    {' '}
                                    Accepted/Previous Tasks
                                </Link>
                            </li>


                            <li className="nav-item">
                                <a
                                    href="javascript:void(0)"
                                    onClick={this.onLogoutClick.bind(this)}
                                    className="nav-link"
                                >
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );


       const adminLinks=(
            <nav className="navbar navbar-expand-sm navbar-dark bg-dark mb-4">
                <div className="container">
                    <Link className="navbar-brand" to="/dashboard">
                        All Tasks
                    </Link>
                    <button
                        className="navbar-toggler"
                        type="button"
                        data-toggle="collapse"
                        data-target="#mobile-nav"
                    >
                        <span className="navbar-toggler-icon" />
                    </button>

                    <div className="collapse navbar-collapse" id="mobile-nav">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item">
                                <Link className="nav-link" to="/tasks">
                                    {' '}
                                    Create Task
                                </Link>
                            </li>


                            <li className="nav-item">
                                <a
                                    href="javascript:void(0)"
                                    onClick={this.onLogoutClick.bind(this)}
                                    className="nav-link"
                                >
                                    Logout
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        );
    

 

        
        return <div>{localStorage.getItem('is_superuser')==='true' ? adminLinks : deliverpersonLinks}</div>
    }
}


const mapStateToProps = state => ({
    auth: state.auth
});

export default withRouter(connect(mapStateToProps, { logoutUser })(
    Navbar
));