import React from 'react';
import moment from 'moment';
import Navbar from './Navbar';
import setAuthToken from '../utils/setAuthToken';
import axios from 'axios';
import isEmpty from '../validation/is-empty';


class TaskListDeliverPerson extends React.Component {
    state = { new_tasks: {}, status: '', errors: {}, network_error: {} };

    componentDidMount() {
        setAuthToken(localStorage.getItem('jwtToken'));

        axios
            .get(`/new_tasks_list_deliver_person`)
            .then(res => {
                this.setState({ new_tasks: res.data });

            })
            .catch(err =>
                console.log(err)
            );

    }
    validate() {
        let errors = {};
        let isValid = true;

        if (this.state.status ==='') {
            errors.status = 'Priority is Required';
            isValid = false;
        }

        if (!isValid) {
            this.setState({ errors: errors });
            return false;
        }
        return true;
    }

    changeStatus(task_id) {
        if (this.validate()) {
            setAuthToken(localStorage.getItem('jwtToken'));
            axios
                .put(`/tasks_action_deliver_person`, { status: this.state.status, task_id: task_id })
                .then(res => {
                    let tasks = this.state.new_tasks;

                    tasks = { ...tasks, status: this.state.status };

                    this.setState({ new_tasks: tasks });
                    this.componentDidMount();

                })
                .catch(err =>
                    // console.log(err.response.data)
                    this.setState({ network_error: { message: err.response.data.message } })
                );
        }
    }
    renderPriority(priority) {
        if (priority === '1')
            return 'LOW';
        else if (priority === '2')
            return 'MEDIUM';
        else if (priority === '3')
            return 'HIGH';
        else
            return 'NO PRIORITY';
    }
    renderActionBtn() {
        const { errors } = this.state;
        return (
            <div>
                <select className='form-control form-control-md'
                    
                    onChange={(e) => this.setState({ status: e.target.value,errors:{},network_error:{} })} >
                    <option value="">Select</option>
                    <option value="accepted">Accept</option>
                </select>

                <span style={{ color: "red" }}>{errors.status}</span>

                <br />
                <button type="button" className="btn btn-info  col-md-6" onClick={this.changeStatus.bind(this, this.state.new_tasks.id)}>Apply</button>

            </div>
        );

        // }
    }

    renderList() {

        const task = this.state.new_tasks;
       
        if(!isEmpty(task)){
            return (
                <tr key={task.id}>
                    <td>{task.title}</td>
                    <td>{this.renderPriority(task.priority)}</td>
                    <td>{task.status.toUpperCase()}</td>
                    <td>{moment(task.created_at).format('YYYY-MM-DD')}</td>
                    <td>{this.renderActionBtn(task)}</td>
                </tr>
    
            );
        }
       

    }
    renderError() {
        if (!isEmpty(this.state.network_error)) {
            return (
                <div className="alert alert-danger alert-dismissible fade show" role="alert">
                    <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong> {this.state.network_error.message}</strong>
                </div>

                
            );
        }
    }

    render() {
        return (
            <div>

                <Navbar />
                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Task Title</th>
                            <th scope="col">Priority</th>
                            <th scope="col">Status</th>
                            <th scope="col">Created At</th>
                            <th scope="col">Action</th>
                        </tr>
                    </thead>
                    <tbody>{this.renderList()}</tbody>
                </table>
                {this.renderError()}


            </div>
        );
    }

}

export default TaskListDeliverPerson;