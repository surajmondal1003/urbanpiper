import React from 'react';
import Navbar from './Navbar';
import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import { Link } from 'react-router-dom';
import _ from 'lodash';
import { withRouter } from "react-router";

class Dashboard extends React.Component {
    state = { tasks: [] };

    componentDidMount() {

        setAuthToken(localStorage.getItem('jwtToken'));
        axios
            .get('/tasks_list')
            .then(res => {

                this.setState({ tasks: res.data });
                console.log(this.state.tasks);
            })
            .catch(err =>
                console.log(err)
            );

    }


    renderDate(created_at) {

        let date = new Date(created_at);
        return date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    }

    cancelTask(task_id) {
        setAuthToken(localStorage.getItem('jwtToken'));
        let tasks = _.map(this.state.tasks, _.cloneDeep);
        const requiredTaskIndex = _.findIndex(tasks, taskItem => taskItem.id === task_id);
        if (requiredTaskIndex > -1) {
            tasks[requiredTaskIndex] = { ...tasks[requiredTaskIndex], status: 'cancelled' };
        }
        axios
            .patch(`/tasks_update/${task_id}`, { status: 'cancelled' })
            .then(res => {
                this.setState({ tasks: tasks }, () => console.log(this.state.tasks));

            })
            .catch(err =>
                console.log(err)
            );
    }

    renderCancelBtn(task) {
        if (task.status !== 'completed' && task.status !== 'declined' && task.status !== 'cancelled') {
            return (
                <div><button type="button" className="btn btn-danger  col-md-6" onClick={this.cancelTask.bind(this, task.id)}>Cancel</button></div>
            );

        }
    }
    renderPriority(priority) {
        if (priority === '1')
            return 'LOW';
        else if (priority === '2')
            return 'MEDIUM';
        else if (priority === '3')
            return 'HIGH';
        else
            return 'NO PRIORITY';
    }
    renderList() {
        return this.state.tasks.map((task) => {
            return (
                <tr key={task.id}>
                    <td>{task.title}</td>
                    <td>{this.renderPriority(task.priority)}</td>
                    <td>{task.status.toString().toUpperCase()}</td>

                    <td>{task.deliver_person ? task.deliver_person.username : 'Not Accepted Yet'}</td>
                    <td>
                        <Link to={`/tasks/${task.id}`} className="btn btn-info  col-md-6">View/Edit</Link>
                        {this.renderCancelBtn(task)}

                    </td>
                </tr>
            );
        });
    }

    render() {

        return (
            <div>
                <Navbar />

                <table className="table">
                    <thead className="thead-dark">
                        <tr>
                            <th scope="col">Task Title</th>
                            <th scope="col">Priority</th>
                            <th scope="col">Status</th>
                            <th scope="col">Delivery Person</th>
                            <th scope="col">Action</th>

                        </tr>
                    </thead>
                    <tbody>
                        {this.renderList()}

                    </tbody>
                </table>


            </div>
        );
    }

}


export default withRouter(Dashboard);