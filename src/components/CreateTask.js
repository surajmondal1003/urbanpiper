import React from 'react';
import Navbar from './Navbar';
import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import { withRouter } from "react-router";

import TaskStatusList from './TaskStatusList';

class CreateTask extends React.Component {

    state = {
        title: '',
        priority: '',
        errors: {},
        task_history: [],
        deliver_person: {}
    };


    componentDidMount() {
        if (this.props.isEdit) {
            setAuthToken(localStorage.getItem('jwtToken'));

            axios.get(`/tasks/${this.props.match.params.id}`)
                .then(res => {
                    this.setState({
                        title: res.data.title,
                        priority: res.data.priority,
                        task_history: res.data.task_status_history,
                        deliver_person: res.data.deliver_person
                    });

                });
        }

    }
    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
        console.log(this.state)
    }

    validate() {
        let errors = {};
        let isValid = true;
        if (this.state.title === '') {
            errors.title = 'Title is Required';
            isValid = false;
        }
        if (this.state.priority === '') {
            errors.priority = 'Priority is Required';
            isValid = false;
        }

        if (!isValid) {
            this.setState({ errors: errors });
            return false;
        }
        return true;
    }

    onSubmit = e => {
        e.preventDefault();
        if (this.validate()) {

            const taskData = {
                title: this.state.title,
                priority: this.state.priority
            };

            setAuthToken(localStorage.getItem('jwtToken'));
            if (this.props.isEdit) {
                axios
                    .put(`/tasks_update/${this.props.match.params.id}`, taskData)
                    .then(res => {
                        console.log(res.data);
                        this.props.history.push('/dashboard');

                    })
                    .catch(err =>
                        console.log(err)
                    );
            } else {
                axios
                    .post('/tasks', taskData)
                    .then(res => {
                        console.log(res.data);
                        this.props.history.push('/dashboard');

                    })
                    .catch(err =>
                        console.log(err)
                    );
            }
        } 

    }

    renderStatus() {
        if (this.props.isEdit) {
            return (
                <TaskStatusList task_history={this.state.task_history} deliver_person={this.state.deliver_person} />
            );
        }
    }

    render() {
        const { errors } = this.state;
        console.log(this.state.errors)
        return (
            <div>
                <Navbar />

                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h1 className="display-4 text-center">{this.props.isEdit ? 'Edit Task' : 'Create Task'}</h1>

                            <form onSubmit={this.onSubmit}>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className='form-control form-control-lg'
                                        placeholder="Task Title"
                                        value={this.state.title}
                                        onChange={(e) => this.setState({ title: e.target.value,errors:{title:''} })} />
                                    <span style={{ color: "red" }}>{errors.title}</span>
                                </div>
                                <div className="form-group">
                                    <select className='form-control form-control-lg'
                                        value={this.state.priority}
                                        onChange={(e) => this.setState({ priority: e.target.value,errors:{priority:''} })} >
                                        <option value="">Select</option>
                                        <option value="1">Low</option>
                                        <option value="2">Medium</option>
                                        <option value="3">High</option>
                                    </select>
                                    <span style={{ color: "red" }}>{errors.priority}</span>
                                </div>


                                <input type="submit" className="btn btn-info btn-block mt-4" />
                            </form>
                        </div>
                    </div>
                </div>
                {this.renderStatus()}
            </div>
        );
    }
}

export default withRouter(CreateTask);